package com.example.marketadmin;

import java.io.Serializable;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Item implements Serializable {
    private boolean soldOut;
    private String key;
        private String title;
        private ArrayList<String>  photoUrl;
        private String madeIn;
        private String size;
        private  int phone;
        private long date;
        private int price;
        private int like=0;
        private int view=0;
    public Item() {
    }


    public void setDate(long date) {
        this.date = date;
    }

    public long getDate() {
        return date;
    }

    public Item(String title, ArrayList<String> photoUrl, String madeIn, String size, int phone, int price, int like, int view, boolean soldOut, long date) {
        this.date=date;
        this.title = title;
        this.photoUrl = photoUrl;
        this.madeIn = madeIn;
        this.size = size;
        this.phone = phone;
        this.price = price;
        this.like = like;
        this.view = view;
        this.soldOut=soldOut;
    }

    public boolean isSoldOut() {
        return soldOut;
    }

    public void setSoldOut(boolean soldOut) {
        this.soldOut = soldOut;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(ArrayList<String> photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getMadeIn() {
        return madeIn;
    }

    public void setMadeIn(String madeIn) {
        this.madeIn = madeIn;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }
}
