package com.example.marketadmin;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;


import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class postNewFragment extends Fragment {


    EditText nameTxt,priceTxt,sizeTxt,madeInTxt,phoneText;
    ImageButton imageButton;
    Button uploadBtn;
    //Firebase
    FirebaseStorage storage;
    StorageReference storageReference;
    int PICK_IMAGE_MULTIPLE = 1;
    String imageEncoded;
    List<String> imagesEncodedList;
    View view;
    private Context context;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ArrayList<Uri>  filePath=new ArrayList<Uri>();
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;


    public postNewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_post_new, container, false);
        context = view.getContext();
        nameTxt=view.findViewById(R.id.item_name_txt);
        priceTxt=view.findViewById(R.id.item_price_txt);
        sizeTxt=view.findViewById(R.id.item_size_txt);
        madeInTxt=view.findViewById(R.id.item_made_txt);
        phoneText=view.findViewById(R.id.item_phone_txt);
        imageView1 = view.findViewById(R.id.img_view1);
        imageView2 = view.findViewById(R.id.img_view2);
        imageView3 = view.findViewById(R.id.img_view3);
        imageView4 = view.findViewById(R.id.img_view4);
        imageButton=view.findViewById(R.id.item_image_button);
        uploadBtn=view.findViewById(R.id.upload_btn);
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference("Items").child("Item");
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();

            }
        });

        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                uploadImage();

            }
        });
        return view;
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,true);
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        try {
            // When an Image is picked
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data




                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();

                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        filePath.add(uri);


                    }


                    if(filePath.size()>3){
                        Picasso.with(getContext()).load(filePath.get(0)).into(imageView1);
                        Picasso.with(getContext()).load(filePath.get(1)).into(imageView2);
                        Picasso.with(getContext()).load(filePath.get(2)).into(imageView3);
                        Picasso.with(getContext()).load(filePath.get(3)).into(imageView4);
                    }
                    else if(filePath.size()>2){
                        Picasso.with(getContext()).load(filePath.get(0)).into(imageView1);
                        Picasso.with(getContext()).load(filePath.get(1)).into(imageView2);
                        Picasso.with(getContext()).load(filePath.get(2)).into(imageView3);

                    }
                    else if(filePath.size()>1){
                        Picasso.with(getContext()).load(filePath.get(0)).into(imageView1);
                        Picasso.with(getContext()).load(filePath.get(1)).into(imageView2);
                    }
                    else {
                        Picasso.with(getContext()).load(filePath.get(0)).into(imageView1);
                    }



                }
            }
            else {
                Toast.makeText(context, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.d("stac", String.valueOf(e.getStackTrace())) ;
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

        super.onActivityResult(requestCode, resultCode, data);

    }
    private void uploadImage() {

        if(filePath.size()>0) {
            final String title = nameTxt.getText().toString().trim();
            final String madeIn = madeInTxt.getText().toString().trim();
            final String size = sizeTxt.getText().toString().trim();

            final int like = 0;
            final int view = 0;
            final boolean soldOut = false;
            final long date = System.currentTimeMillis();
            if (title.isEmpty()) {
                nameTxt.setError("title is required");
                nameTxt.requestFocus();
                return;
            }
            if (priceTxt.getText().toString().trim().isEmpty()) {
                priceTxt.setError("price is required");
                priceTxt.requestFocus();
                return;
            }
            if (phoneText.getText().toString().trim().isEmpty()) {
                phoneText.setError("phone is required");
                phoneText.requestFocus();
                return;
            }
            if (phoneText.getText().toString().trim().length() != 10) {
                phoneText.setError("phone length must be 10 digits");
                phoneText.requestFocus();
                return;
            }
            if (size.isEmpty()) {
                sizeTxt.setError("size is required");
                sizeTxt.requestFocus();
                return;
            }
            if (madeIn.isEmpty()) {
                madeInTxt.setError("made in is required");
                madeInTxt.requestFocus();
                return;
            }



            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            final ArrayList<Uri> downloadUri = new ArrayList<>();
            final ArrayList<String> photoUrl = new ArrayList<>();

            for (int i = 0; i < filePath.size() - 1; i++) {
                StorageReference ref = storageReference.child("images/" + UUID.randomUUID().toString());
                ref.putFile(filePath.get(i)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Task<Uri> urlTask = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                        while (!urlTask.isSuccessful()) ;

                        photoUrl.add(String.valueOf(urlTask.getResult()));
                    }


                })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                        .getTotalByteCount());
                                progressDialog.setMessage("Uploaded " + (int) progress + "%");
                            }
                        });
            }
            StorageReference ref = storageReference.child("images/" + UUID.randomUUID().toString());

                ref.putFile(filePath.get(filePath.size() - 1))
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Task<Uri> urlTask = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                                while (!urlTask.isSuccessful()) ;
                                downloadUri.add(urlTask.getResult());
                                photoUrl.add(String.valueOf(urlTask.getResult()));

                                final int phone = Integer.parseInt(phoneText.getText().toString().trim());
                                final int price = Integer.parseInt(priceTxt.getText().toString().trim());
                                Item item = new Item(title, photoUrl, madeIn, size, phone, price, like, view, soldOut, date);
                                databaseReference.push().setValue(item);
                                nameTxt.setText("");
                                madeInTxt.setText("");
                                sizeTxt.setText("");
                                phoneText.setText("");
                                priceTxt.setText("");
                                imageView1.setImageResource(R.drawable.ic_image_icon_black_24dp);
                                imageView2.setImageResource(R.drawable.ic_image_icon_black_24dp);
                                imageView3.setImageResource(R.drawable.ic_image_icon_black_24dp);
                                imageView4.setImageResource(R.drawable.ic_image_icon_black_24dp);
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "file Uploaded", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                        .getTotalByteCount());
                                progressDialog.setMessage("Uploaded " + (int) progress + "%");
                            }
                        });
            }


        else {
            Toast.makeText(context,"please select photos",Toast.LENGTH_SHORT).show();
        }
    }
}
