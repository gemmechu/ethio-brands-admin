package com.example.marketadmin;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class Menu1 extends Fragment {
    private List<Item> dataList = new ArrayList<>();
    //private ItemAdapter mAdapter;
    private FirebaseAuth mAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private DatabaseReference mdatabaseLike;
    private RecyclerView recyclerView;
    ProgressBar progressBar;
    static Context context;
    private boolean processLike=false;
    FirebaseRecyclerAdapter firebaseRecyclerAdapter;
    Query mRecentQuery;
    Item item;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,@Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_menu_1, container, false);
        //   recyclerView.setAdapter(mAdapter);
        firebaseDatabase= FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        databaseReference=firebaseDatabase.getReference("Items").child("Item");
        mdatabaseLike=firebaseDatabase.getReference("Items").child("Likes");
         mRecentQuery = databaseReference.orderByKey();
        context=getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.list_recycler_view);
        progressBar=view.findViewById(R.id.wait_progressbar);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseRecyclerAdapter= new FirebaseRecyclerAdapter<Item, ItemViewHolder>(
                Item.class,R.layout.list_item,ItemViewHolder.class,databaseReference

        ) {

            @Override
            public Item getItem(int position) {
                return super.getItem(getItemCount() - 1 - position);
            }
            @Override
            protected void populateViewHolder(ItemViewHolder viewHolder, Item model, final int position) {
                model =getItem(position);
                final String post_key= getRef(getItemCount() - 1 - position).getKey();
                viewHolder.setItemTitle(model.getTitle());
                viewHolder.setItemPrice(String.valueOf(model.getPrice()) );

                viewHolder.setItemLike(String.valueOf(model.getLike()) );
                viewHolder.setItemViews(String.valueOf(model.getView()) );
                viewHolder.setItemSize(model.getSize());
                viewHolder.setItemMade(model.getMadeIn());
                viewHolder.setSoldOut(model.isSoldOut());
               viewHolder.setImageView(model.getPhotoUrl().get(0));
                viewHolder.setPhoneBtnTxt(String.valueOf(model.getPhone()));
                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getChildrenCount()>0){
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent i = new Intent(v.getContext(), DescriptionActivity.class);
                        i.putExtra("itemId", post_key);

                        startActivity(i);
                    }
                });

            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        View mView;
        public TextView itemTitle,itemPrice,itemViews,itemLike,itemSize,itemMade,soldOut;
        public ImageView imageView;
        ImageButton likeBtn;
        DatabaseReference mdatabaseLike;
        FirebaseAuth mAuth;
        public Button phoneBtn;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            mView=itemView;
            mAuth = FirebaseAuth.getInstance();
            mdatabaseLike=FirebaseDatabase.getInstance().getReference("Items").child("Likes");
            mdatabaseLike.keepSynced(true);
            phoneBtn= (Button)itemView.findViewById(R.id.phone_btn);


            phoneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String phone="tel:"+phoneBtn.getText().toString().trim();
                    Uri uri = Uri.parse(phone);
                    Intent it = new Intent(Intent.ACTION_DIAL, uri);
                    context.startActivity(it);
                }
            });
        }
        public void setSoldOut(boolean sold){
            soldOut=itemView.findViewById(R.id.sold_txt);
            if(sold){
                soldOut.setVisibility(View.VISIBLE);
            }
            else{
                soldOut.setVisibility(View.INVISIBLE);
            }
        }

        public void setPhoneBtnTxt(String phoneBtn) {
            this.phoneBtn.setText("0"+phoneBtn);
        }

        public void setItemTitle(String title){
            itemTitle=(TextView) itemView.findViewById(R.id.item_title);
            itemTitle.setText(title);
        }
        public void setItemPrice(String title){
            itemPrice=(TextView) itemView.findViewById(R.id.item_price_top);
            itemPrice.setText(title);
        }
        public void setItemSize(String title){
            itemSize=(TextView) itemView.findViewById(R.id.item_size);
            itemSize.setText(title);
        }
        public void setItemMade(String title){
            itemMade=(TextView) itemView.findViewById(R.id.item_made);
            itemMade.setText(title);
        }
        public void setItemViews(String title){
            itemViews=(TextView) itemView.findViewById(R.id.item_view);
            itemViews.setText(title);
        }
        public void setItemLike(String title){
            itemLike=(TextView) itemView.findViewById(R.id.item_like);
            itemLike.setText(title);
        }
        public void setImageView(String url){
            imageView= (ImageView)itemView.findViewById(R.id.item_image);
            Picasso.with(context).load(url).into(imageView);
        }


    }
}
