package com.example.marketadmin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PICK_IMAGE = 1;
    EditText nameTxt,priceTxt,sizeTxt,madeInTxt,phoneText;
    ImageButton imageButton;private ImageView imageView;
    Button editBtn;
    CheckBox soldOut;
    Item item;String itemKey;
    ProgressBar editProgress;
    private DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        databaseReference= FirebaseDatabase.getInstance().getReference("Items").child("Item");
        editProgress=findViewById(R.id.edit_progress);
        editProgress.setVisibility(View.INVISIBLE);
        inflateViews();
         itemKey=getIntent().getExtras().getString("postKeyToBeEdited");
        item= (Item) getIntent().getSerializableExtra("oldItem");
        populateData(itemKey,item);


    }

    private void inflateViews(){
        nameTxt=findViewById(R.id.edit_name_txt);
        priceTxt=findViewById(R.id.edit_price_txt);
        sizeTxt=findViewById(R.id.edit_size_txt);
        madeInTxt=findViewById(R.id.edit_made_txt);
        phoneText=findViewById(R.id.edit_phone_txt);
        imageView = findViewById(R.id.edit_img_view);
        imageButton=findViewById(R.id.edit_image_button);
        editBtn=findViewById(R.id.edit_item_btn);
        editBtn.setOnClickListener(this);
        soldOut=findViewById(R.id.sold_check_box);
    }

    private void populateData(String itemKey,Item item) {
      if(item!=null) {
            nameTxt.setText(item.getTitle());
            priceTxt.setText(String.valueOf(item.getPrice()));
            sizeTxt.setText(item.getSize());
            madeInTxt.setText(item.getMadeIn());
            phoneText.setText(String.valueOf(item.getPhone()) );
            soldOut.setChecked(item.isSoldOut());
            Picasso.with(EditActivity.this).load(item.getPhotoUrl().get(0)).into(imageView);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.edit_item_btn:

                String title=nameTxt.getText().toString();
                int price= Integer.parseInt(priceTxt.getText().toString());
                String size=sizeTxt.getText().toString();
                String madeIn=madeInTxt.getText().toString();
                int phone= Integer.parseInt(phoneText.getText().toString());
                boolean sold= soldOut.isChecked();
                ArrayList<String> photoUrl=item.getPhotoUrl();
                long date=item.getDate();
                Item itemNew =  new Item(title,  photoUrl,  madeIn,  size,  phone,  price, item.getLike(), item.getView(),sold,date);
                updateItem(itemKey,itemNew);
                break;

        }
    }

    private void updateItem(String itemKey, Item item) {
        editProgress.setVisibility(View.VISIBLE);
        databaseReference.child(itemKey).setValue(item).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                editProgress.setVisibility(View.INVISIBLE);
                Toast.makeText(EditActivity.this,getString(R.string.updateSuccess),Toast.LENGTH_SHORT).show();
            }
        });
    }
}
