package com.example.marketadmin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.support.v4.content.ContextCompat.startActivity;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ListViewHolder> implements Filterable {
    private List<Item> dataList;
    private Context context;
    private List<Item> dataListFiltered;
    private ListAdapterListener listener;

    public ItemAdapter(List<Item> dataList) {
        this.dataList=dataList;

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFiltered = dataList;
                } else {
                    List<Item> filteredList = new ArrayList<>();
                    for (Item row : dataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getTitle().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    dataListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFiltered;
                return filterResults;
            }
            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFiltered = (ArrayList<Item>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ListViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        public TextView itemTitle,itemPrice,itemViews,itemLike,itemSize,itemMade;
        public ImageView imageView;
        public Button phoneBtn;
        public ListViewHolder(final View view) {
            super(view);
            itemPrice=(TextView) itemView.findViewById(R.id.item_price_top);
            itemTitle=(TextView) itemView.findViewById(R.id.item_title);
            itemViews=(TextView) itemView.findViewById(R.id.item_view);
            itemLike=(TextView) itemView.findViewById(R.id.item_like);
            itemSize=(TextView) itemView.findViewById(R.id.item_size);
            itemMade=(TextView) itemView.findViewById(R.id.item_made);
            imageView= (ImageView)itemView.findViewById(R.id.item_image);
            phoneBtn= (Button)itemView.findViewById(R.id.phone_btn);
            imageView.setOnClickListener(this);
            phoneBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            System.out.println("id:  "+v.getId());
            switch (v.getId()){

                case R.id.item_image:
//                    String title =itemTextView.getText().toString().trim();
//                    double price =Double.parseDouble("123") ;
//                    //int picturePath =(int)itemImageView.getTag();
//                    int picturePath= (int) itemImageView.getId();
//                    int phoneNum = Integer.parseInt(phoneBtn.getText().toString().trim()) ;
//                    Item item= new Item(title,  picturePath,  phoneNum,  price);
//                    System.out.println("rsource: "+itemImageView.getResources());
//                    Intent i = new Intent(v.getContext(), DescriptionActivity.class);
//
//                    i.putExtra("clickedItem",  item);
//
//                    v.getContext().startActivity(i);

                    break;
                case  R.id.phone_btn:
                    String phone=phoneBtn.getText().toString().trim();
                    Uri uri = Uri.parse("tel:8005551234");
                    Intent it = new Intent(Intent.ACTION_DIAL, uri);
                    v.getContext().startActivity(it);
                    break;


            }
        }
    }
//    public ListAdapter(Context context, List<Item> ourData, ListAdapterListener listAdapterListener) {
//        this.dataList = ourData;
//        this.dataListFiltered=ourData;
//        this.context=context;
//        this.listener=listAdapterListener;
//    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        context = parent.getContext();
        return new ListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, int i) {
        Item item = dataList.get(i);
        listViewHolder.itemPrice.setText(String.valueOf(item.getPrice()));
        listViewHolder.itemTitle.setText(item.getTitle());
        listViewHolder.itemSize.setText(item.getSize());
        listViewHolder.itemMade.setText(item.getMadeIn());

        listViewHolder.itemViews.setText(String.valueOf((item.getView())));
        listViewHolder.itemLike.setText(String.valueOf(item.getLike()));
      //  listViewHolder.imageView.setImageURI(Uri.parse(item.getPhotoUrl()));
       // Picasso.with(this.context).load(item.getPhotoUrl()).into(listViewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
    public interface ListAdapterListener {
        void onContactSelected(Item item);
    }

}