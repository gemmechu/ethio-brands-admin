package com.example.marketadmin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class DescriptionActivity extends AppCompatActivity implements View.OnClickListener {
    TextView itemPriceDetail,itemTitleDetail,itemSizeDetail,itemMadeInDetail,itemLikeDetail,itemViewDetail;
    Button itemRemoveBtn,itemEditBtn;
    ImageView imageView;
    private String itemKey=null;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private FirebaseAuth mAuth;
    Item item;
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        imageView=findViewById(R.id.image_view_description);
        firebaseDatabase= FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        databaseReference=firebaseDatabase.getReference("Items").child("Item");
        itemTitleDetail=findViewById(R.id.item_title_detail);
        itemRemoveBtn=findViewById(R.id.remove_btn);
        itemPriceDetail=findViewById(R.id.item_price_details);
        itemSizeDetail=findViewById(R.id.item_size_detail);
        itemMadeInDetail=findViewById(R.id.item_made_in_details);
        itemLikeDetail=findViewById(R.id.item_like_detail);
        itemViewDetail=findViewById(R.id.item_view_detail);
        itemEditBtn=findViewById(R.id.edit_btn);
        itemKey= getIntent().getExtras().getString("itemId");
        databaseReference.child(itemKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                item= dataSnapshot.getValue(Item.class);
                if(item!=null) {
                    String title = (String) dataSnapshot.child("title").getValue();
                    itemTitleDetail.setText(item.getTitle());
                    itemPriceDetail.setText(String.valueOf(item.getPrice()));
                    itemSizeDetail.setText(item.getSize());
                    itemMadeInDetail.setText(item.getMadeIn());
                    itemLikeDetail.setText(String.valueOf(item.getLike()));
                    itemViewDetail.setText(String.valueOf(item.getView()));
                    Log.d("img",item.getPhotoUrl().get(0));
                    Picasso.with(DescriptionActivity.this).load(item.getPhotoUrl().get(0)).into(imageView);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        itemEditBtn.setOnClickListener(this);
        itemRemoveBtn.setOnClickListener(this);






    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.edit_btn:
                Intent intent= new Intent(DescriptionActivity.this,EditActivity.class);
                intent.putExtra("postKeyToBeEdited",itemKey);
                intent.putExtra("oldItem",item);
                startActivity(intent);
                break;
            case R.id.remove_btn:
                databaseReference.child(itemKey).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(DescriptionActivity.this,"Item successfully removed",Toast.LENGTH_SHORT).show();
                    }
                });
                Intent i= new Intent(DescriptionActivity.this,MainActivity.class);
                startActivity(i);
                break;

        }
    }
}
